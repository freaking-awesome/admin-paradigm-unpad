import Head from 'next/head'
import Link from 'next/link'

export default function DefaultLayout({ children }: any) {
  return (
    <>
      <Head>
        <title>Admin Paradigm</title>
      </Head>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container">
          <Link href="/">
            <span className="navbar-brand cursor-pointer">Paradigm Unpad</span>
          </Link>
          <button
            className="navbar-toggler cursor-pointer"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item active">
                <Link href="/">
                  <span className="nav-link cursor-pointer">Home</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/participant">
                  <span className="nav-link cursor-pointer">Participants</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/refferal">
                  <span className="nav-link cursor-pointer">Refferal</span>
                </Link>
              </li>
              <li className="nav-item">
                <a className="nav-link cursor-pointer" href="#">
                  Logout
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      {children}
    </>
  )
}
