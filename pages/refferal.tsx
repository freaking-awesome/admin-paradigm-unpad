import DefaultLayout from '../layouts/default'
import { generate } from '../utils/random-string'
import Modal from 'react-bootstrap/Modal'
import { Button, Form } from 'react-bootstrap'
import { useState } from 'react'

export default function Refferal() {
  const [showModal, setShowModal] = useState(false)
  const [form, setForm] = useState({
    name: '',
    refferalCode: '',
    status: true,
  })

  const handleInputChange = (e: any) => {
    setForm({
      ...form,
      [e.target.id]: e.target.value,
    })
  }

  const handleSubmitForm = (e: any) => {
    e.preventDefault()

    console.log(form)

    setShowModal(false)
  }

  return (
    <>
      <Modal
        show={showModal}
        size="lg"
        centered
        onHide={() => setShowModal(false)}
      >
        <Modal.Header>
          <Modal.Title>Create New Refferal Code</Modal.Title>
        </Modal.Header>

        <form onSubmit={handleSubmitForm}>
          <Modal.Body>
            <Form.Group>
              <Form.Label>Name</Form.Label>
              <Form.Control
                id="name"
                onChange={handleInputChange}
                type="text"
                placeholder="Fullname"
              />
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            <Button type="submit" variant="primary">
              Save
            </Button>
          </Modal.Footer>
        </form>
      </Modal>
      <DefaultLayout>
        <div className="container mt-5">
          <div className="row mb-3">
            <div className="col-4 float-right">
              <button
                className="btn btn-primary"
                onClick={() => setShowModal(true)}
              >
                Add New Refferal Code
              </button>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <table className="table table-hovered">
                <thead>
                  <tr>
                    <th>Admin Name</th>
                    <th>Refferal Code</th>
                    <th>Discount</th>
                    <th>Status</th>
                  </tr>
                </thead>

                <tbody>
                  <tr>
                    <td>Fulan</td>
                    <td>
                      {generate(4)}-{generate(4)}-{generate(4)}
                    </td>
                    <td>40</td>
                    <td>
                      <div className="custom-control custom-switch">
                        <input
                          type="checkbox"
                          className="custom-control-input"
                        />
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </DefaultLayout>
    </>
  )
}
