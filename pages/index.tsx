import type { NextPage } from 'next'
import Link from 'next/link'
import DefaultLayout from '../layouts/default'

const Home: NextPage = () => {
  return (
    <>
      <DefaultLayout>
        <div className="container mt-5">
          <div className="row">
            <div className="col-md-3">
              <div className="card" style={{ width: '18rem' }}>
                <div className="card-body">
                  <h5 className="card-title">Events</h5>
                  <p className="card-text">4</p>
                </div>
              </div>
            </div>

            <div className="col-md-3">
              <div className="card" style={{ width: '18rem' }}>
                <div className="card-body">
                  <h5 className="card-title">Participants</h5>
                  <p className="card-text">4</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </DefaultLayout>
    </>
  )
}

export default Home
