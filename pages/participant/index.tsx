import DefaultLayout from '../../layouts/default'

export default function Participants() {
  return (
    <>
      <DefaultLayout>
        <div className="container mt-5">
          <table className="table table-hovered">
            <thead>
              <tr>
                <th>ID</th>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Institution</th>
                <th>Major</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
      </DefaultLayout>
    </>
  )
}

export async function getServerSideProps() {
  return {
    props: {},
  }
}
